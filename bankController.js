import express      from 'express';
import VError       from 'verror';
import Promise      from 'bluebird';
import request      from 'request-promise';
import cheerio      from 'cheerio';
import _            from 'lodash';
import promiseRetry from 'promise-retry';

import { systemMethod }       from '../middlewares/acl';
import { dbs }                from '../../../mongoDB';
import { getAllBanks }        from '../services/centralBankService';
import SearchService          from '../../../common/services/searchIndexService';
import { BANKS_TIMEOUT }      from '../../../constants';
import { bankLogger }         from '../../../logger';
import config                 from '../../../config';
import bankRates              from '../../../config/bankRates';
import esSettings             from '../../../config/es.banks.rates';

const elasticConfig  = config.get('elasticSearch');
const elasticService = new SearchService(
  elasticConfig.config,
  elasticConfig.indexBanks.indexName,
  elasticConfig.indexBanks.types.rates);

const retrySettings = {
  retries:    5,
  factor:     1.5,
  maxTimeout: 300000,
  randomize:  true
};

const router = express.Router();

const Bank   = dbs.torgbox.Bank;

/*
 * Метод загрузки рейтингов банков с raexpert и acra-ratings
 *
 * Это фоновый воркер и он не обязан возвращать полезную инфу, но обязан писать все в лог (!)
 */
function downloadBanks(req, res, next) {
  res.responses.success();

  const options = {
    timeout:   BANKS_TIMEOUT,
    uri:       '',
    transform: (body) => cheerio.load(body)
  };

  // Запрашиваем все банки с сайта Центрального Банка РФ
  return getAllBanks()
  // получаем ИНН
    .then(banks => {
      const ogrns = banks.map(bank => bank.OGRN);

      return Bank.find({ ogrn: { $in: ogrns } }, 'inn').lean()
        .then(data => {
          data = _.keyBy(data, 'OGRN');

          let ogrn;

          banks.map(bank => {
            ogrn = bank.OGRN;

            if (data[ogrn]) bank.INN = data[ogrn].inn;
          });
        })
        .catch(err => {
          bankLogger.error(`Ошибка при получении ИНН по банкам: ${err}`);
        });
    })
    .then(banks => {
      bankLogger.info('INN - success');

      if (!banks.length) return banks;

      const withoutINN = banks.filter(el => !el.INN);

      banks = banks.filter(el => el.INN);
      banks = _.keyBy(banks, 'INN');
      // запрашиваем список банков с raexpert - это на данный момент одна страница
      options.uri = 'https://raexpert.ru/ratings/bankcredit/';

      return request(options)
        .then($ => {
          const raBanks = $('.ratinglist-table >tbody>tr:not([rel="rating_row_callback"])').map((i, el) => {
            const rate = $(el).find('.rating-img-container').text().replace(/^ru(.+)$/, '$1').replace(/^(.+)\(RU\)$/, '$1');

            return {
              link: $(el).find('a.ratinglist-table-link').first().attr('href'),
              rates: {
                raexpert: {
                  value: rate.toLowerCase() !== 'отозван' ? $(el).find('.rating-img-container').text() : '',
                  order: rate.toLowerCase() !== 'отозван' ? bankRates.indexOf(rate) : 0
                }
              }
            };
          }).toArray();

          bankLogger.info('RA: первый запрос - success');

          return raBanks;
        })
        // запрашиваем информацию по каждому банку с raexpert
        .mapSeries(bank => {
            options.uri = `https://raexpert.ru${bank.link}`;

            delete bank.link;

            return promiseRetry(retrySettings, (retry) => {
              return request(options)
                .then($ => {
                  const titles = $('.company-info-numbers-item').find('.company-info-name').toArray().map(e => $(e).text());
                  const values = $('.company-info-numbers-item').find('.company-info-num').toArray().map(e => $(e).text());

                  if (!titles.length && !values.length) return retry(new Error());

                  let index = titles.indexOf('ИНН:');
                  bank.INN  = values[ index ].trim();

                  banks[ bank.INN ] = Object.assign(bank, banks[ bank.INN ]);
                })
                .catch(retry);
            })
              .then(response => response,
                err => bankLogger.error(`Ошибка при загрузке данных по банку ${bank.name} с raexpert: ${err}`)
              );
          },
          { concurrency: 5 }
        )
        .then(() => bankLogger.info('RA: дополнительные запросы - success'))
        // запрашиваем список банков с acra-ratings, учитывая пагинацию
        .then(() => {
          return new Promise((resolve) => {
            let page      = 1;
            let acraBanks = [];

            function requestPage() {
              options.uri = `https://www.acra-ratings.ru/ratings/issuers?order=title&page=${page}&q=&sort=desc&subgroups%5B%5D=18`;

              return request(options)
                .then($ => {
                  const temp = $('.l-search-results__list-items').map((i, el) => {
                    const rateValue = $(el).find('>td:nth-child(2) span:not(.l-rating-optional-title)').remove('.l-rating-remark').text();
                    const cleanRate = rateValue.replace(/^ru(.+)$/, '$1').replace(/^(.+)\(R.+$/, '$1');

                    return {
                      //name:           $(el).find('.l-search-results__item').first().text(),
                      link: $(el).find('.l-search-results__item').first().attr('href'),
                      value: cleanRate.toLowerCase() !== 'отозван' ? rateValue : '',
                      order: cleanRate.toLowerCase() !== 'отозван' ? bankRates.indexOf(cleanRate) : 0
                    };
                  }).toArray();

                  if (!temp.length) {
                    bankLogger.info('Acra: первый запрос - success');

                    return resolve(acraBanks);
                  }

                  acraBanks = acraBanks.concat(temp);
                  ++page;

                  return requestPage();
                })
                .catch(err => {
                  bankLogger.error(`Ошибка при загрузке данных с acra-ratings, страница ${page}: ${err}`);

                  return resolve(acraBanks);
                });
            }

            return requestPage();
          });
        })
        // запрашиваем информацию по каждому банку с acra-ratings
        .mapSeries(bank => {
            options.uri = `https://www.acra-ratings.ru${bank.link}`;

            delete bank.link;

            return request(options)
              .then($ => {
                bank.INN = $($('.l-rating_tabel_item-value').get(2)).text().trim();

                if (banks[ bank.INN ]) {
                  if (!banks[ bank.INN ].rates) banks[ bank.INN ].rates = {};

                  banks[ bank.INN ].rates.acra = { value: bank.value, order: bank.order };
                }
              })
              .catch(err => bankLogger.error(`Ошибка при загрузке данных по банку ${bank.name} с acra-ratings: ${err}`))
          },
          { concurrency: 5 }
        )
        .then(() => {
          bankLogger.info('Acra: дополнительные запросы - success');

          return Object.values(banks).concat(withoutINN);
        })
        .catch(err => {
          bankLogger.error(`Ошибка при загрузке данных с raexpert: ${err}`);

          return err instanceof Error ? err : new VError(err);
        });
    })
    .then(banks => {
      if (!banks.length) return '';

      const operations = banks.reduce((op, bank) => {
        if (bank.status === 'лицензия отозвана') delete bank.license;

        delete bank.code;
        delete bank.status;

        return op.concat(
          {
            index: {
              _index: elasticConfig.indexBanks.indexName,
              _type:  elasticConfig.indexBanks.types.rates
            }
          },
          bank
        );
      }, []);

      return elasticService.ifExists()
        .then(result => {
          if (result) return elasticService.deleteIndex();
        })
        .then(() => elasticService.createIndex(esSettings))
        .then(() => {
          elasticService.bulk(
            operations,
            (err, resp) => {
              if (err || resp.errors) {
                let message = '';

                if (resp.errors) {
                  message = resp.items.filter(item => item.index.error).reduce((mes, elem)=> {
                    elem = elem.index.error;
                    return mes +=`${elem.type} - ${elem.reason} - ${elem.caused_by.type} - ${elem.caused_by.reason}\n`;
                  }, message);
                }

                bankLogger.error(`Ошибка при загрузке данных в ElasticSearch: ${err ? err.message : message}`);
              } else bankLogger.info('ES: запись - success');

              return;
            },
            elasticConfig.bulkAddTimeout
          );
        })
        .catch(err => {
          bankLogger.error(`Ошибка при работе с ElasticSearch: ${err}`);

          return err instanceof Error ? err : new VError(err);
        });
    })
    .catch(err => {
      bankLogger.error(`Ошибка при загрузке данных с raexpert: ${err}`);

      return err instanceof Error ? err : new VError(err);
    });
}

router.post('/',          systemMethod,                   downloadBanks);