import Promise      from 'bluebird';
import request      from 'request-promise';
import xmljs        from 'xml-js';
import { Iconv }    from 'iconv';
import _            from 'lodash';
import promiseRetry from 'promise-retry';

import { bankLogger } from '../../../logger';
import {
  CBR_API_URI,
  CBR_SCRIPT_URI,
  BANKS_TIMEOUT
}                     from '../../../constants';

const iconv = new Iconv('windows-1251', 'UTF-8');

const retrySettings = {
  retries:    5,
  factor:     1.5,
  maxTimeout: 300000,
  randomize:  true
};

function cbrRequest(action, body) {
  const options = {
    method:  'POST',
    uri:     CBR_API_URI,
    headers: {
      'Content-Type': 'text/xml; charset=utf-8',
      SOAPAction:     action
    },
    timeout: BANKS_TIMEOUT,
    body
  };

  return promiseRetry(retrySettings, (retry) => {
    return request(options)
      .then(response => {
        response = xmljs.xml2json(response, { compact: true, ignoreAttributes: true, trim: true });

        if (JSON.parse(response)._declaration._attributes.encoding !== 'utf-8') {
          response = iconv.convert(response).toString('utf8');
        }

        response = JSON.parse(response);

        return response[ 'soap:Envelope' ][ 'soap:Body' ];
      })
      .catch(retry);
  })
    .then(response => response,
      err => {
        bankLogger.error(`Ошибка при запросе ЦБР ${action}: ${err.message ? err.message : err}`);

        throw new Error();
      });
}

function getAllBanks() {
  let body = '';

  // получаем перечень всех банков В СБР
  return request({
    uri:      CBR_SCRIPT_URI,
    timeout:  BANKS_TIMEOUT,
    encoding: null
  })
    .then(response => {
      response = xmljs.xml2json(response, { compact: true, ignoreAttributes: true, trim: true });

      /*if (JSON.parse(response)._declaration._attributes.encoding !== 'utf-8')  {
        response = iconv.convert(response).toString('utf8');
      }*/

      response = JSON.parse(response);

      let banks = response.BicCode.Record.map(bank => ({ BIC: bank.Bic._text }));

      bankLogger.info('Получен список всех банков');

      return banks;
    })
    // Для каждого из них получаем внутренний код
    .mapSeries(bank => {
        body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <BicToIntCode xmlns="http://web.cbr.ru/">
      <BicCode>${bank.BIC}</BicCode>
    </BicToIntCode>
  </soap:Body>
</soap:Envelope>`;

        return cbrRequest('http://web.cbr.ru/BicToIntCode', body)
          .then(response => {
            bank.code = response.BicToIntCodeResponse.BicToIntCodeResult._text;

            return bank;
          });
      },
      { concurrency: 1 }
    )
    // Получаем информацию пачками по 100 банков
    .then(banks => {
      bankLogger.info('Получены внутренние коды банков');

      const codes = banks.map(({code}) => code);

      banks = _.keyBy(banks, 'code');

      return Promise.mapSeries(_.chunk(codes, 100), chunk => {
        body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CreditInfoByIntCodeExXML xmlns="http://web.cbr.ru/">
      <InternalCodes> 
        ${chunk.reduce((requestString, code) => requestString += `<double>${code}</double>`, '')}
      </InternalCodes>
    </CreditInfoByIntCodeExXML>
  </soap:Body>
</soap:Envelope>`;

        return cbrRequest('http://web.cbr.ru/CreditInfoByIntCodeExXML', body)
          .then(response => {
            response = response.CreditInfoByIntCodeExXMLResponse.CreditInfoByIntCodeExXMLResult.CreditOrgInfo.CO;
            response.map(info => {
              const index = info.IntCode._text;

              if (!info.MainRegNumber) {
                bankLogger.warn(`У банка \"${info.OrgName._text}\" отсутствует ОГРН, он будет выкинут из списка обработки`);

                delete banks[ index ];

                return;
              }

              banks[ index ].license   = info.RegNumber._text;
              banks[ index ].OGRN      = info.MainRegNumber._text;
              banks[ index ].status    = info.OrgStatus._text;
              banks[ index ].name      = info.OrgFullName._text;
              banks[ index ].shortName = info.OrgName._text;
            });
          });
      })
        .then(() => Object.values(banks))
        // Получаем дату для формы 123
        .then(banks => {
          bankLogger.info('Получена дополнительная информация для всех банков');

          let date;
          const withoutLicense = banks.filter(el => el.status === 'лицензия отозвана');

          banks = banks.filter(el => el.status !== 'лицензия отозвана');

          // Тут есть опасность, что у первого в списке банка дата будет не "свежей", мы тааких ниже проверяем.
          // Проблема в том, что мы не можем предсказать в начале месяца, когда появится отчет по предыдущему месяцу
          body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetDatesForF123 xmlns="http://web.cbr.ru/">
      <CredprgNumber>${banks[0].license}</CredprgNumber>
    </GetDatesForF123>
  </soap:Body>
</soap:Envelope>`;

          return cbrRequest('http://web.cbr.ru/GetDatesForF123', body)
            .then(response => {
              response = response.GetDatesForF123Response.GetDatesForF123Result.dateTime;

              date = response.pop()._text;
              return banks;
            })
            // Достаем капитал для каждого банка
            .mapSeries(bank => {
                body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <Data123FormFullXML xmlns="http://web.cbr.ru/">
      <CredorgNumber>${bank.license}</CredorgNumber>
      <OnDate>${date}</OnDate>
    </Data123FormFullXML>
  </soap:Body>
</soap:Envelope>`;

                return cbrRequest('http://web.cbr.ru/Data123FormFullXML', body)
                  .then(response => {
                    let capital = response.Data123FormFullXMLResponse.Data123FormFullXMLResult.F123DATA.F123.shift();

                    if (capital.VALUE) bank.capital = Math.abs(capital.VALUE._text);

                    return bank;
                  });
              },
              { concurrency: 1 }
            )
            .then(() => {
              bankLogger.info('Получены формы 123 для всех банков');

              return banks.concat(withoutLicense)
            });
        });
    })
    .catch(err => {
      if (err.message) bankLogger.error(`Ошибка при получении всех банков с ЦБР: ${err.message}`);

      return [];
    });
}

module.exports = { getAllBanks };